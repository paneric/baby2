define(
	[
		'modules/main/core/scene'
	]
	,function(objRender) {

		return function() {

			//canvas/window resize event handler, like this
			window.addEventListener('resize', function() {
				objRender.engine.resize();
			});
		}
	}
);