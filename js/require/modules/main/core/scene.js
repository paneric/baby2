define(
	[
		'modules/main/core/canvas'
		,'modules/main/core/engine'
		,'modules/main/core/render'
	]
	, function(objCanvas, objEngine, funRender, funAnimate) {

		var objRender = {
			scene : new BABYLON.Scene(objEngine),
			engine : objEngine
		};

		if (BABYLON.Engine.isSupported()) {
			BABYLON.SceneLoader.Append(
				'js/require/modules/main/'
				,'main.babylon'
				,objRender.scene
				,funRender(objRender)
				,function (progress){}
			);
		}
		
//console.log(objRender.scene.meshes[0]);
		return objRender;
	}
);
