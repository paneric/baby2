define(
	[
		'modules/main/core/canvas'
	]
	, function(objCanvas) {

		return new BABYLON.Engine(objCanvas);
	}
);