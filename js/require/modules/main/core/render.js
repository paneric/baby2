define(
	[
		'modules/main/events'
	]
	, function(funEvents) {

	return function(objRender) {
		objRender.scene.executeWhenReady(function () {

			//initialize the event register class after creating the scene:
			var nativeEventsHandler = new BABYLONX.EventsRegister(objRender.scene);
			funEvents(objRender);

			// Once the scene is loaded, just register a render loop to render it
			objRender.engine.runRenderLoop(function() {
				objRender.scene.render();
			});
		});
	}
});
