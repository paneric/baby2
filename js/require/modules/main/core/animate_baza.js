define([], function() {

		return function(objRender) {
console.log(objRender);
			var iconHelp = objRender.scene.meshes[0];
			var iconEula = objRender.scene.meshes[1];
			var iconSettings = objRender.scene.meshes[5];
			var iconStatus = objRender.scene.meshes[6];
			
			var animationBox0 = new BABYLON.Animation("tutoAnimation","rotation.y",30,BABYLON.Animation.ANIMATIONTYPE_FLOAT,
			BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
			
			var animationBox1 = new BABYLON.Animation("tutoAnimation","rotation.y",30,BABYLON.Animation.ANIMATIONTYPE_FLOAT,
			BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
			
			var animationBox5 = new BABYLON.Animation("tutoAnimation","rotation.y",30,BABYLON.Animation.ANIMATIONTYPE_FLOAT,
			BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);
			
			var animationBox6 = new BABYLON.Animation("tutoAnimation","rotation.y",30,BABYLON.Animation.ANIMATIONTYPE_FLOAT,
			BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT);

			
			var keys = [];  
 
			//At the animation key 0, the value of scaling is "1"
			keys.push({
				frame: 0,
				value: 0
			});
 
			//At the animation key 100, the value of scaling is "1"
			keys.push({
				frame: 10,
				value:Math.PI/4
			});
			
			animationBox0.setKeys(keys);
			animationBox1.setKeys(keys);
			animationBox5.setKeys(keys);
			animationBox6.setKeys(keys);
			
			iconHelp.animations.push(animationBox0);
			iconEula.animations.push(animationBox1);
			iconSettings.animations.push(animationBox5);
			iconStatus.animations.push(animationBox6);
			
			objRender.scene.beginAnimation(iconHelp, 0, 100, true);
			objRender.scene.beginAnimation(iconEula, 0, 100, true);
			objRender.scene.beginAnimation(iconSettings, 0, 100, true);
			objRender.scene.beginAnimation(iconStatus, 0, 100, true);
			
			// Once the scene is loaded, just register a render loop to render it
			objRender.engine.runRenderLoop(function() {
				objRender.scene.render();
			});
		}
	}
);